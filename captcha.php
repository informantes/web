<?php 
	header('Access-Control-Allow-Origin: *');
	require_once "recaptchalib.php";

	// your secret key
	$secret = "6Lc72AYUAAAAADMmqqy0JIGAlPGddoE2Ec8l-th_";
	 
	// empty response
	$response = null;
	 
	// check secret key
	$reCaptcha = new ReCaptcha($secret);

	if ($_POST["response"]) {
	    $response = $reCaptcha->verifyResponse(
	        $_SERVER["REMOTE_ADDR"],
	        $_POST["response"]
	    );
	}

	if ($response != null && $response->success) {
	    echo true;
	  } else {
	  	echo false;
	  }
?>