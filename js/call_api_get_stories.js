
function checkBlockCountries()
{
    var testCountry = locationData.country
    var countryObj = spanishSpeakingCountries.filter(function ( countrySelected )
                { return countrySelected.name.toLocaleLowerCase() == testCountry.toLocaleLowerCase(); })[0];
    if (typeof countryObj == 'undefined') { if(event) { location.href = "noaccess"; } }
}

// General function for API GET Stories
function callAPIGETStories(isGetAPIByIP)
{
	// Endpoint is variable, depends for isGetAPIByIP value
	// if isGetAPIByIP, GET API by IP
	// else, GET API by Locations, with Lat and Long
	var endpoint = "";
	if(typeof params.page == 'undefined' ) { currentPage = 0; }
	else { currentPage = params.page; }
	
	if(typeof params.type == 'undefined' ) { currentType = 0; }
	else { currentType = params.type; }


	if(isGetAPIByIP)
	{
		// If the stories are requested via an IP Address,
		// is because we don’t know the users location
		// console.log("call API GET BY IP");
		callIPByIP = true;
		if( typeof params.code == 'undefined' )
		{
			var isCityInEndpoint = (currentPage == 0) ? null : escape(params.city);
			if(typeof params.currenttype != 'undefined' )
			{
				endpoint = apiBaseEndpoint + "stories/"+ispIP+"/"+ params.currenttype+"/"+currentPage;
			}
			else
			{
				endpoint = apiBaseEndpoint + "stories/"+ispIP+"/"+ currentType+"/"+currentPage;
			}
		}
		else
		{
			var isCityInEndpoint = (currentPage == 0) ? null : escape(params.city);
			endpoint = apiBaseEndpoint + "stories/"+params.lat+"/"+params.lng+"/0/"+ currentPage +"/"+isCityInEndpoint ;
		}
	}
	else
	{
		// If the stories are requested via the users location,
		// is because we know his latitude and longitud position
		// console.log("call API GET BY LOCATION");
		callIPByIP = false;
		var isCityInEndpoint = (currentPage == 0) ? null : escape(params.city);
		endpoint = apiBaseEndpoint + "stories/"+latitude+"/"+longitude+"/0/"+ currentPage +"/"+ isCityInEndpoint;

		//endpoint = "api/rorschach/stories/6.2530408/-75.5645737/0/1/"+ currentPage;
	}

	if(ispIP == null) { setTimeout(function() { callAPIGETStories(isGetAPIByIP); }, 1000); }
	else
	{
		$.ajax({
		type: "GET",
			url: urlBase + endpoint,
			crossDomain: true,
			success: function(response) { fillStoriesInformation(response, callIPByIP); },
			error: function(error) { console.log(error); }
		});
	}
}

function callAPIForCountry()
{
	currentType = 1;
	$("#noStories").addClass("hide"); $("#ajaxLoader").removeClass("hide");
	var endpoint = apiBaseEndpoint + "stories/"+ispIP+"/"+ currentType +"/"+currentPage;
	$.ajax({
		type: "GET",
			url: urlBase + endpoint,
			crossDomain: true,
			success: function(response)
			{
				if(response.data.length > 0)
				{
					fillStoriesInformation(response, true);
				}
				else
				{
					$("#noStories").removeClass("hide"); $("#ajaxLoader").addClass("hide");
					$(".historyRegisterCity").addClass("hide");
				}
			},
			error: function(error) { console.log(error); }
		});
}

function showNoStoriesMessage(city, country)
{
	$("#noStories").removeClass("hide"); $("#ajaxLoader").addClass("hide");
	setTimeout(function()
	{
		var historyRegisterCity = $(".historyRegisterCity");
		var cityNoStories = $(".cityNoStories");

		if(typeof city == 'undefined')
		{
			historyRegisterCity.text("Ver #"+locationData.country);
			cityNoStories.text(locationData.city);
		}
		else
		{
			historyRegisterCity.text("Ver #"+country);
			cityNoStories.text(city);
		}
		
	}, 1500);
}

function getFormatedEmojiNumber(increase)
{
	if(increase >= 1000 && increase < 1000000)
	{
		increase = parseFloat(increase /1000).toFixed(2).substr(0, 3);
		if(increase.split(".")[1] == 0 ) { increase = increase.split(".")[0] + "K"; }
		else { increase = increase  + "K" }
	}
	else if(increase >= 1000000)
	{
		increase = parseFloat(increase /1000000).toFixed(2).substr(0, 3);
		if(increase.split(".")[1] == 0 ) { increase = increase.split(".")[0] + "M"; }
		else { increase = increase  + "M" }
	}
	return increase;
}

// Fill information with GET API Data
function fillStoriesInformation(response, callIPByIP)
{
	locationData.city = response.city;
	locationData.country = response.country;
	
	// City url param is not defined
	if (callIPByIP)
	{
		$("#showCity").addClass("hide");
		if(locationData.country != "" && locationData.country != null)
		{
			$($("#showCountry")[0]).html("#"+decodeURI(locationData.country));
		}
	}
	// City url param is defined
	else
	{
		$(".crimeShowCity.pipe.hidePipe").removeClass("hidePipe");
		if( typeof params.city == 'undefined' &&
			typeof params.country == 'undefined' &&
			typeof params.lat == 'undefined' &&
			typeof params.lng == 'undefined' )
		{
			$($("#showCity")[0]).html("#"+decodeURI(locationData.city)+" ");
			$($("#showCountry")[0]).html("#"+decodeURI(locationData.country));
		}
		else
		{
			$($("#showCity")[0]).html("#"+decodeURI(params.city)+" ");
			if(params.country == "" || typeof params.country == 'undefined')
			{
				$(".crimeShowCity.pipe").css("opacity", "0");
			}
			else
			{
				$($("#showCountry")[0]).html("#"+decodeURI(params.country));
			}
		}
	}

	// Check if user are able to open informant web page
	// This will be validate first before load informant webpage
	// checkBlockCountries();

	// There is no crimes to show
	// Show no Stories Message
	if(response.code == 101)
	{
		if(window.location.pathname == '/web/') { showNoStoriesMessage(decodeURI(params.city), decodeURI(params.country), 0); }
		else
		{
			$($("#showCity")[0]).html("#"+decodeURI(params.city)+" ");
			$($("#showCountry")[0]).html("#"+decodeURI(params.country));
			noCases(params.city, params.country);
		}
	}

	// Success cases
	if(response.code == 200)
	{
		var responseLength = response.data.length;
		var targetInsertStories = $("#casesStories");
		var targetAjaxLoader = $("#ajaxLoader");

		// Activate More Stories Button
		if(typeof city == 'undefined') { city = locationData.city }
		if(typeof country == 'undefined') { country = locationData.country }

		if(responseLength == 0) { showNoStoriesMessage(city, country); }
		if(responseLength > 0) { targetInsertStories.removeClass("hide"); targetAjaxLoader.addClass("hide"); }

		if(responseLength >= 20 && response.final_page == 0)
		{
			setTimeout(function(){ $("#moreHistories").removeClass("hide"); }, 500);
		}
		else if(responseLength == 0 && response.final_page == 1)
		{
			noCases(locationData.city, locationData.country);
		}

		response.data.forEach(function (story)
		{
			if(story.story == null || story.story == "")
			{
				// Avoid Stories Here
			}
			else
			{
				var reactions = [];
				
				var crimename = crimes.filter(function ( crimeSelected )
					{ return crimeSelected.id === story.crime_id; })[0];

				var theDateSplited = story.date.split(" ");
				var theDate = theDateSplited[0] + "T" + theDateSplited[1];
				var crimedate = moment(theDate).format('MMM DD, YYYY HH:mm');


				var seeMapLink = "http://maps.google.com/maps?q=loc:"+ story.latitude +","+ story.longitude;

				reactions.push({ id:1, count: story.reaction_1, name: "Es triste" });
				reactions.push({ id:2, count: story.reaction_2, name: "Mal, muy mal" });
				reactions.push({ id:3, count: story.reaction_3, name: "Uy que rabia" });
				reactions.push({ id:4, count: story.reaction_4, name: "Cooomo?" });
				reactions.push({ id:5, count: story.reaction_5, name: "No puede ser!" });
				reactions.push({ id:6, count: story.reaction_6, name: "Pura m!@#$" });
				reactions.push({ id:7, count: story.reaction_7, name: "Que raro…" });
				reactions.push({ id:8, count: story.reaction_8, name: "Sin comentarios" });

				reactions.sort(function(a, b) { return parseFloat(b.count) - parseFloat(a.count); });

				var emojiFirst = reactions[0];
				var emojiSecond = reactions[1];
				var emojiThird = reactions[2];
				var emojiFourth = reactions[3];
				var emojiFifth = reactions[4];
				var emojiSixth = reactions[5];
				var emojiSeventh = reactions[6];
				var emojiEighth = reactions[7];

				emojiFirst.countFormated = getFormatedEmojiNumber(emojiFirst.count);
				emojiSecond.countFormated = getFormatedEmojiNumber(emojiSecond.count);
				emojiThird.countFormated = getFormatedEmojiNumber(emojiThird.count);
				emojiFourth.countFormated = getFormatedEmojiNumber(emojiFourth.count);
				emojiFifth.countFormated = getFormatedEmojiNumber(emojiFifth.count);
				emojiSixth.countFormated = getFormatedEmojiNumber(emojiSixth.count);
				emojiSeventh.countFormated = getFormatedEmojiNumber(emojiSeventh.count);
				emojiEighth.countFormated = getFormatedEmojiNumber(emojiEighth.count);

				// Call API Google for address
				var googleMapsAPIEndpoint =
					"http://maps.googleapis.com/maps/api/geocode/json?latlng="
					+ story.latitude +","
					+ story.longitude +"&sensor=true";
						
				var storyID = "historyRegister" + story.id;

				var htmlStory =
				'<div id='+ storyID +' class="historyRegister hasHistory">\
					<div class="historyContent col-sm-12">\
						<div class="crimeIconAndDesc">\
							<div class="row">\
								<div class="crimeShowIcon">\
									<img src="img/ic-crime-'+ story.crime_id +'.png" alt="ic-crime-agression" />\
								</div>\
								<p>'+ crimename.name +'<span class="crimeShowDate">'+ crimedate +'</span></p>\
							</div>\
							<div class="row crimeDescription">';
							if(story.author == null || story.author == "")
							{
								htmlStory = htmlStory + story.story;
							}
							else
							{
								htmlStory = htmlStory + '<i>Por <span class="italicBold">'+ story.author +'.</span></i> '+ story.story;
							}

							var theAddress;
							if(typeof params.city == 'undefined')
							{
								theAddress = (typeof story.address == 'undefined' || story.address == null) ? story.city : story.address
							}
							else
							{
								theAddress = (typeof story.address == 'undefined' || story.address == null) ? decodeURI(params.city) : story.address
							}
							

							htmlStory = htmlStory + '</div>\
							<div class="row crimeDescription">\
								<i><span class="italicBold">Ocurrió en</span></i> <span>' + theAddress + '</span> <a href="' + seeMapLink + '" target="_blank">ver mapa</a>\
							</div>\
							<div class="row crimeDescription">\
								<a class="share" href="https://twitter.com/intent/tweet?\
										original_referer=https%3A%2F%2Finformant.es%2F%3Fid%3D12345&\
										ref_src=twsrc%5Etfw&\
										text='+ ( story.author = (story.author == null || story.author == "") ? "Anónimo" : story.author ) +'%20ha%20compartido%20una%20historia%20de%20la%20ciudad%20de%20%23'+ locationData.city +'%20en%20%23informantes%20&\
										tw_p=tweetbutton&\
										url=https%3A%2F%2Finformant.es%2F%3Fid%3D'+story.id+'" target="_blank" class="twitter-share-button" data-text="" data-hashtags="informantes" data-dnt="true">Compartir</a>\
								<div id="shareSocialMedia'+ story.id +'" class="ShareSocialMedia">\
									<span class="socialMediaBtn btnFacebook fb-share-button">\
										<a href="https://www.facebook.com/sharer.php?\
											u=https%3A%2F%2Finformant.es%2F%3Fid%3D'+story.id+'" target="_blank">Facebook</a>\
									</span>\
									<button class="socialMediaBtn btnTwitter">\
										<a href="https://twitter.com/intent/tweet?\
										original_referer=https%3A%2F%2Finformant.es%2F%3Fid%3D12345&\
										ref_src=twsrc%5Etfw&\
										text='+ story.author +'%20ha%20compartido%20una%20historia%20de%20la%20ciudad%20de%20%23'+ locationData.city +'%20en%20%23informantes%20&\
										tw_p=tweetbutton&\
										url=https%3A%2F%2Finformant.es%2F%3Fid%3D'+story.id+'" target="_blank" class="twitter-share-button" data-text="" data-hashtags="informantes" data-dnt="true">Tweet</a>\
									</button>\
								</div>\
								<div class="shareReactions storyCards">\
									<div class="row">\
										<div class="emojiBox fullView" onclick="openReactionsBox(\'reactionBox' + story.id + '\')">\
											<div class="emojiImg">\
												<img src="img/ic-add-reaction@2x.png" alt="add-reaction" />\
											</div>\
										</div>\
										<div id="reactionBox'+ story.id +'" class="reactionBox">\
											<div class="row">\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', ' + emojiFirst.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiFirst.id +'@2x.png" alt="emoji-'+ + emojiFirst.id +'" />\
													</div>\
													<span class="emoji'+ emojiFirst.id +'" val="'+ emojiFirst.count+ '">'+ emojiFirst.countFormated +'</span>\
												</div>\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiSecond.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiSecond.id +'@2x.png" alt="emoji-'+ emojiSecond.id +'" />\
													</div>\
													<span class="emoji'+ emojiSecond.id +'" val="'+ emojiSecond.count +'">'+ emojiSecond.countFormated +'</span>\
												</div>\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiThird.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiThird.id +'@2x.png" alt="emoji-'+ emojiThird.id +'" />\
													</div>\
													<span class="emoji'+ emojiThird.id +'" val="'+ emojiThird.count +'">'+ emojiThird.countFormated +'</span>\
												</div>\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiFourth.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiFourth.id +'@2x.png" alt="emoji-'+ emojiFourth.id +'" />\
													</div>\
													<span class="emoji'+ emojiFourth.id +'" val="'+ emojiFourth.count +'">'+ emojiFourth.countFormated +'</span>\
												</div>\
											</div>\
											<div class="row">\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', ' + emojiFifth.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiFifth.id +'@2x.png" alt="emoji-'+ + emojiFifth.id +'" />\
													</div>\
													<span class="emoji'+ emojiFifth.id +'" val="'+ emojiFifth.count+ '">'+ emojiFifth.countFormated +'</span>\
												</div>\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiSixth.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiSixth.id +'@2x.png" alt="emoji-'+ emojiSixth.id +'" />\
													</div>\
													<span class="emoji'+ emojiSixth.id +'" val="'+ emojiSixth.count +'">'+ emojiSixth.countFormated +'</span>\
												</div>\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiSeventh.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiSeventh.id +'@2x.png" alt="emoji-'+ emojiSeventh.id +'" />\
													</div>\
													<span class="emoji'+ emojiSeventh.id +'" val="'+ emojiSeventh.count +'">'+ emojiSeventh.countFormated +'</span>\
												</div>\
												<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiEighth.id +', ' + story.id + ')">\
													<div class="emojiImg">\
														<img src="img/emoji-'+ emojiEighth.id +'@2x.png" alt="emoji-'+ emojiEighth.id +'" />\
													</div>\
													<span class="emoji'+ emojiEighth.id +'" val="'+ emojiEighth.count +'">'+ emojiEighth.countFormated +'</span>\
												</div>\
											</div>\
										</div>\
										';

										if(emojiFirst.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', ' + emojiFirst.id +', ' + story.id + ')" sort="'+emojiFirst.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiFirst.id +'@2x.png" alt="emoji-'+ + emojiFirst.id +'" />\
											</div>\
											<span class="emoji'+ emojiFirst.id +'" val="'+ emojiFirst.count+ '">'+ emojiFirst.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiSecond.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiSecond.id +', ' + story.id + ')" sort="'+emojiSecond.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiSecond.id +'@2x.png" alt="emoji-'+ emojiSecond.id +'" />\
											</div>\
											<span class="emoji'+ emojiSecond.id +'" val="'+ emojiSecond.count +'">'+ emojiSecond.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiThird.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiThird.id +', ' + story.id + ')" sort="'+emojiThird.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiThird.id +'@2x.png" alt="emoji-'+ emojiThird.id +'" />\
											</div>\
											<span class="emoji'+ emojiThird.id +'" val="'+ emojiThird.count +'">'+ emojiThird.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiFourth.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiFourth.id +', ' + story.id + ')" sort="'+emojiFourth.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiFourth.id +'@2x.png" alt="emoji-'+ emojiFourth.id +'" />\
											</div>\
											<span class="emoji'+ emojiFourth.id +'" val="'+ emojiFourth.count +'">'+ emojiFourth.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiFifth.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiFifth.id +', ' + story.id + ')" sort="'+emojiFifth.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiFifth.id +'@2x.png" alt="emoji-'+ emojiFifth.id +'" />\
											</div>\
											<span class="emoji'+ emojiFifth.id +'" val="'+ emojiFifth.count +'">'+ emojiFifth.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiSixth.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiSixth.id +', ' + story.id + ')" sort="'+emojiSixth.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiSixth.id +'@2x.png" alt="emoji-'+ emojiSixth.id +'" />\
											</div>\
											<span class="emoji'+ emojiSixth.id +'" val="'+ emojiSixth.count +'">'+ emojiSixth.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiSeventh.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiSeventh.id +', ' + story.id + ')" sort="'+emojiSeventh.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiSeventh.id +'@2x.png" alt="emoji-'+ emojiSeventh.id +'" />\
											</div>\
											<span class="emoji'+ emojiSeventh.id +'" val="'+ emojiSeventh.count +'">'+ emojiSeventh.countFormated +'</span>\
										</div>\
										';
										}

										if(emojiEighth.count != 0)
										{
											htmlStory = htmlStory + '<div class="emojiBox" onclick="addCounter(\'' + storyID + '\', '+ emojiEighth.id +', ' + story.id + ')" sort="'+emojiEighth.count+'">\
											<div class="emojiImg">\
												<img src="img/emoji-'+ emojiEighth.id +'@2x.png" alt="emoji-'+ emojiEighth.id +'" />\
											</div>\
											<span class="emoji'+ emojiEighth.id +'" val="'+ emojiEighth.count +'">'+ emojiEighth.countFormated +'</span>\
										</div>\
										';
										}

									htmlStory = htmlStory + '</div>\
								</div>\
								<a class="report" onclick="report('+ story.id +')">Reportar</a>\
							</div>\
						</div>\
					</div>\
				</div>';

				targetInsertStories.append(htmlStory);
			}
		});
	}
}

// Mark Stories as Reported
function report(targetId)
{
	swal({
		title: "Reportar contenido",
		text: "Por la naturaleza del servicio, este contenido te puede parecer inapropiado ¿Aún deseas reportarlo?.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#339bc0",
		cancelButtonColor: "#339bc0",
		confirmButtonText: "Si",
		cancelButtonText: "No",
		closeOnConfirm: true
	},
	function(event)
	{
		// Case user press Yes
		if(event)
		{
			var params =  { story_id :targetId, phone_id: 0 };

			$.ajax({
				type: "POST",
				url: urlBase + apiBaseEndpoint + "flags",
				data: params,
				success: function(response)
				{
					if(response.code == 200) {  }
					else
					{
						setTimeout(function()
						{
							swal({
								title: "Error al reportar la historia ",
								text: error,
								type: "error",
								showCancelButton: false,
								confirmButtonColor: "#339bc0",
								confirmButtonText: "Cerrar",
								customClass: "swalCustom",
								closeOnConfirm: false
							});
						}, 1000);
					}
				},
				error: function(error)
				{
					setTimeout(function()
					{
						swal({
							title: "Error al reportar la historia ",
							text: error,
							type: "error",
							showCancelButton: false,
							confirmButtonColor: "#339bc0",
							confirmButtonText: "Cerrar",
							customClass: "swalCustom",
							closeOnConfirm: false
						});
					}, 1000);
				}
			});	
		}
	});
}

// Use sort to redefine reactions
function getSorted(selector, attrName) {
    return $($(selector).toArray().sort(function(a, b){
        var aVal = parseInt(a.getAttribute(attrName)),
            bVal = parseInt(b.getAttribute(attrName));
        return bVal - aVal;
    }));
}

function addCounterPostStory(idNumber, keyEmoji, storyID)
{
	if(!isBusy)
	{
		isBusy = true;

		var reactionData =  { story_id: storyID, reaction: keyEmoji };
		var target = $("#" + idNumber +" .emoji"+keyEmoji);
		var increase = 1 + parseInt(target.attr("val"));
		target.attr("val", increase);
		target.parent().attr("sort", increase);
		
		$.ajax({
			type: "POST",
			url: urlBase + apiBaseEndpoint + "reactions",
			data: reactionData,
			success: function(response)
			{
				if(response.code === 200)
				{
					var target = $("#" + idNumber +" .emoji"+keyEmoji);
					var increase = 0 + parseInt(target.attr("val"));
					target.attr("val", increase);
					
					// remove onclick once click
					target.parent().attr("onclick", "");	

					if(increase >= 1000 && increase < 1000000)
					{
						increase = parseFloat(increase /1000).toFixed(2).substr(0, 3);
						if(increase.split(".")[1] == 0 ) { increase = increase.split(".")[0] + "K"; }
						else { increase = increase  + "K" }
					}
					else if(increase >= 1000000)
					{
						increase = parseFloat(increase /1000).toFixed(2).substr(0, 3);
						if(increase.split(".")[1] == 0 ) { increase = increase.split(".")[0] + "M"; }
						else { increase = increase  + "M" }
					}
				}
				isBusy = false;
				target.text(increase);
			},
			error: function(error)
			{
				swal({
					title: "Error al agregar la reacción",
					text: error,
					type: "error",
					showCancelButton: false,
					confirmButtonColor: "#339bc0",
					confirmButtonText: "Cerrar",
					customClass: "swalCustom",
					closeOnConfirm: false
				});
				isBusy = false;
			}
		});
	}
}

var isBusy = false;
// Add Emoji Counter
function addCounter(idNumber, keyEmoji, storyID)
{
	if(!isBusy)
	{
		isBusy = true;

		var reactionData =  { story_id: storyID, reaction: keyEmoji };
		var maxReactionNumber = parseInt($($("#reactionBox93483 .row .emojiBox span")[0]).attr("val"));

		var addNewReactionItem = '\
			<div class="emojiBox" sort="1">\
				<div class="emojiImg">\
					<img src="img/emoji-'+ keyEmoji +'@2x.png" alt="emoji-'+ keyEmoji +'" />\
				</div>\
				<span class="emoji'+ keyEmoji +'" val="1">1</span>\
			</div>\
			';

		var targetAddNewReaction = "#historyRegister" + storyID + " .crimeIconAndDesc .shareReactions > .row";
		var totalEmojiInserted = $("#historyRegister" + storyID + " .crimeIconAndDesc .shareReactions > .row > .emojiBox").length;

		var target = $("#" + idNumber +" .emoji"+keyEmoji);
		var increase = 1 + parseInt(target.attr("val"));
		target.attr("val", increase);
		target.parent().attr("sort", increase);
		
		$.ajax({
			type: "POST",
			url: urlBase + apiBaseEndpoint + "reactions",
			data: reactionData,
			success: function(response)
			{
				if(response.code === 200)
				{
					var target = $("#" + idNumber +" .emoji"+keyEmoji);
					var increase = 0 + parseInt(target.attr("val"));
					target.attr("val", increase);
					
					// remove onclick once click
					target.parent().attr("onclick", "");

					if(increase >= 1000)
					{
						increase = parseFloat(increase /1000).toFixed(2).substr(0, 3);
						if(increase.split(".")[1] == 0 ) { increase = increase.split(".")[0] + "K"; }
						else { increase = increase  + "K" }
					}

					// Remove current reaction and move
					if(totalEmojiInserted < 5 && increase == 1)
					{
						$(targetAddNewReaction).append(addNewReactionItem);
					}
					else
					{
						var mySelector = $("#historyRegister" + storyID + " .shareReactions > .row > .emojiBox:not(.fullView)");
						var selectorToAppend = $("#historyRegister" + storyID + " .shareReactions > .row");
						var newSort = getSorted(mySelector, 'sort');
						mySelector.remove();
						selectorToAppend.append(newSort);
					}

					if( $("#reactionBox"+storyID).hasClass("showSocial") )
					{
						setTimeout(function()
						{
							openReactionsBox("reactionBox"+storyID);
							isBusy = false;
						}, 300);
						
					}
				}
				isBusy = false;
				target.text(increase);
			},
			error: function(error)
			{
				swal({
					title: "Error al agregar la reacción",
					text: error,
					type: "error",
					showCancelButton: false,
					confirmButtonColor: "#339bc0",
					confirmButtonText: "Cerrar",
					customClass: "swalCustom",
					closeOnConfirm: false
				});
				isBusy = false;
			}
		});
	}
}

var busyMoreStories = false;

// Load More Stories
function moreStories()
{
	if(!busyMoreStories)
	{
		busyMoreStories = true;
		currentPage ++;
		if(typeof params.city != 'undefined' && typeof params.country != 'undefined')
		{
			params.page = currentPage;
			var paramsAsStringURL = jQuery.param( params );

			location.href = "more_cases?"+paramsAsStringURL;
		}
		else
		{
			if(locationData.city == "" || locationData.city == null)
			{
				location.href = "more_cases?page="+currentPage+
					"&city="+escape(locationData.country)+
					"&currenttype="+currentType;
			}
			else
			{
				location.href = "more_cases?page="+currentPage+"&city="+escape(locationData.city)+
				"&currenttype="+currentType;
			}
			
		}
		
	}
}

// Open Reaction box
function openReactionsBox(targetId)
{
	var targetShare = $("#"+targetId);
	if(targetShare.hasClass("showSocial")) { targetShare.removeClass("showSocial"); }
	else { targetShare.addClass("showSocial"); }
	
}

// Open Stories By City comming from index
function openStoriesByCity(target)
{
	var city = $(target).html().replace("#", "").toLowerCase();
	var country = locationData.country;
	var latitude = thePlace.geometry.location.lat();
	var longitude = thePlace.geometry.location.lng();
	
	window.open("more_cases?city=" + city +
		"&country=" + country +
		"&lat=" + latitude +
		"&lng=" + longitude);
}

function openStoriesLocation(target, myLatitude, myLongitude, myCity, myCountry)
{
	var openUrl =
		"more_cases?page=0" +
		"&city=" + escape(myCountry) +
		"&currenttype=0" ;

	document.location.href = openUrl;
}