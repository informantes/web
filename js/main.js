
(function() { "use strict"; })();

var reactions = [];

var onSuccess = function(response) {
	globalResponse = response;
	var errorDivs = document.getElementsByClassName("recaptcha-error");
	if (errorDivs.length) {
		errorDivs[0].className = "";
	}
	var errorMsgs = document.getElementsByClassName("recaptcha-error-message");
	if (errorMsgs.length) {
		errorMsgs[0].parentNode.removeChild(errorMsgs[0]);
	}
};

function validateStoryId()
{
	var modalHeader = $(".modal-header");
	if(typeof params.id != 'undefined' )
	{
		var endpoint = apiBaseEndpoint + "stories/"+params.id;

		$.ajax({
			type: "GET",
			url: urlBase + endpoint,
			crossDomain: true,
			success: function(theStory)
			{
				var crime = theStory.data.crime_id;
				var crimeObj = crimes.filter(function ( crimeSelected )
						{ return crimeSelected.id == crime; })[0];

				var seeMapLink = "http://maps.google.com/maps?q=loc:"+ theStory.data.latitude +","+ theStory.data.longitude;

				var name = $("#name").val();
				var story = $("#story").val();

				var theDateSplited = theStory.data.date.split(" ");
				var theDate = theDateSplited[0] + "T" + theDateSplited[1];

				var crimeDateForamted = moment(theDate).format('MMM DD, YYYY HH:mm');
				var emojiBoxes = $("#myReactions .emojiBox");

				//$("#crimeShowIcon img").attr("src", "img/ic-crime-'+ theStory.data.crime_id +'.png");
				var targetCrimeIcon = "img/ic-crime-"+ theStory.data.crime_id +".png"
				$("#customModal .crimeShowIcon img").attr("src", targetCrimeIcon);

				// Fill Popup For with Data
				$($("#crimename")[0]).html(
				crimeObj.name +'<span class="crimeShowDate">'
				+ crimeDateForamted +'</span>');

				if(theStory.data.author == null)
				{
					$($("#storyInfo")[0]).html(theStory.data.story);
				}
				else
				{
					$($("#storyInfo")[0]).html(
					'<i>Por <span class="italicBold">'
					+ theStory.data.author +'.</span></i> '
					+ theStory.data.story);
				}

				var theAddress = (typeof story.address == 'undefined' || theStory.data.address == null  || theStory.data.address == "") ? theStory.city : theStory.data.address

				$($("#storyLocation")[0]).html(
				'<i><span class="italicBold">Ocurrió en</span></i> <span>'
				+ theAddress + '</span> <a href="'
				+ seeMapLink + '" target="_blank">ver mapa</a>');

				$($("#storyCity")[0]).html(
				'#'+decodeURI(theStory.city.latinize()));
				var targetStoryCity = "openStoriesLocation(this, '"
					+ theStory.data.latitude + "', '"
					+ theStory.data.longitude + "', '"
					+ theStory.city.latinize() + "',  '"
					+ theStory.country.latinize() + "' )";

				$("#storyCity").attr("onclick", targetStoryCity);

				var twitterAppend = '<div class="crimeShowIcon">\
					<span class="socialMediaBtn btnTwitter">\
						<a href="https://twitter.com/intent/tweet?\
						original_referer=https%3A%2F%2Finformant.es%2F%3Fid%3D12345&\
						ref_src=twsrc%5Etfw&text='+ (theStory.data.author = (story.author == null) ? "Anónimo" : theStory.data.author ) +'%20ha%20compartido%20una%20historia%20de%20la%20ciudad%20de%20%23medellin%20en%20%23informantes%20&\
						tw_p=tweetbutton&url=https%3A%2F%2Finformant.es%2F%3Fid%3D'+ params.id +'" target="_blank" class="twitter-share-button" data-text="" data-dnt="true">twitter</a>\
					</span>\
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');\
					</script>\
				</div>';

				$(".historySocialMedia").append(twitterAppend);

				emojiBoxes.each(function(key, emoji)
				{
					var keyEmoji = (key + 1)
					var targetEmoji = "emoji" + keyEmoji;
					var targetEventOnClick = "addCounterPostStory('myReactions', '"
					+ keyEmoji + "', "+ params.id + ")";

					$(emoji).attr("onclick", targetEventOnClick);
				});

				$($("#visits")[0]).html(theStory.visits);
				$($("#similars")[0]).html(theStory.similar);

				$($("#myReactions .emoji1")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_1));
				$($("#myReactions .emoji2")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_2));
				$($("#myReactions .emoji3")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_3));
				$($("#myReactions .emoji4")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_4));
				$($("#myReactions .emoji5")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_5));
				$($("#myReactions .emoji6")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_6));
				$($("#myReactions .emoji7")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_7));
				$($("#myReactions .emoji8")[0]).html(getFormatedEmojiNumber(theStory.data.reaction_8));

				$("#myReactions .emoji1").attr("val", theStory.data.reaction_1);
				$("#myReactions .emoji2").attr("val", theStory.data.reaction_2);
				$("#myReactions .emoji3").attr("val", theStory.data.reaction_3);
				$("#myReactions .emoji4").attr("val", theStory.data.reaction_4);
				$("#myReactions .emoji5").attr("val", theStory.data.reaction_5);
				$("#myReactions .emoji6").attr("val", theStory.data.reaction_6);
				$("#myReactions .emoji7").attr("val", theStory.data.reaction_7);
				$("#myReactions .emoji8").attr("val", theStory.data.reaction_8);

				if(!modalHeader.hasClass("hide")) { modalHeader.addClass("hide"); }
				$('#customModal').modal('toggle');
			},
			error: function(error) {
				swal({
					title: "Error al consultar los datos de la historia",
					text: "No fue posible establecer la conexión con el servidor",
					type: "warning",
					showCancelButton: false,
					confirmButtonColor: "#339bc0",
					cancelButtonColor: "#339bc0",
					confirmButtonText: "OK",
					cancelButtonText: "No",
					closeOnConfirm: false
				},
				function(event)
				{
					if(event) { location.reload(); }
				});
				console.log(error);
			}
		});
	}
}
validateStoryId();

function validateForm()
{
	if(!isBusy)
	{
		isBusy = true;

		var canContinue = true;
		var validationConcat = "";

		var crime = $("#crime").val();
		var crimeObj = crimes.filter(function ( crimeSelected )
				{ return crimeSelected.id == crime; })[0];
		var crimeDate = $("#crimeDate").val().replace("-", "").replace("-", "");
		var crimeHour = $("#crimeHour").val();
		var fullDateAndHour = $("#crimeDate").val() + "T" + crimeHour;
		var name = $("#name").val();
		var story = $("#story").val();
		var address = $("#pac-input").val();

		if(crime == null) { canContinue = false; validationConcat += "Crime"; }

		if(typeof globalPlaces == 'undefined') { canContinue = false; validationConcat += " Location"; }
		else if(globalPlaces == null) { canContinue = false; validationConcat += " Location"; }

		if(crimeDate == "") { canContinue = false; validationConcat += " Date"; }
		if(crimeHour == "") { canContinue = false; validationConcat += " Time"; }
		if(globalResponse == null) { canContinue = false; validationConcat+= " Captcha"; }
		if(name == "" || name == null) { name = "Anónimo"; }

		if(canContinue)
		{
			var seeMapLink = "http://maps.google.com/maps?q=loc:"+ globalPlaces.lat() +","+ globalPlaces.lng();

			var captchaData = { response: globalResponse };
			$('html#informant').addClass('overflowHidden');

			$.ajax({
				type: "POST",
				url: urlBase + "captcha.php",
				data: captchaData,
				success: function(response)
				{
					if(response == 1)
					{
						// Define endpoint url
						var url = urlBase + apiBaseEndpoint + "stories";

						// Define data object for API
						var data =
						{
							crime_id: crime,
							latitude: globalPlaces.lat(),
							longitude: globalPlaces.lng(),
							address: address,
							date_id: crimeDate,
							time: crimeHour,
							did_police_showed_up: null,
							did_crime_was_resolved: null,
							story: story,
							author: name,
							phone_id: null,
							user_ip: ispIP,
							source: navigator.userAgent
						};

						if(callIPByIP) {
							data.user_latitude = null;
							data.user_longitude = null;
						}else {
							data.user_latitude = latitude;
							data.user_longitude = longitude;
						}

						$.ajax({
							type: "POST",
							url: url,
							data: data,
							dataType: "json",
							success: function(response)
							{
								var myStory = response;
								console.log(myStory);
								var emojiBoxes = $("#myReactions .emojiBox");

								if(response.code === 200)
								{
									setTimeout(function()
									{
										var targetCrimeIcon = "img/ic-crime-"+ data.crime_id +".png"
										$("#customModal .crimeShowIcon img").attr("src", targetCrimeIcon);


										$("#crime").val("1");
										$("#pac-input, #crimeDate, #crimeHour").val("");
										$('#customModal').modal('toggle');

										var crimeDateForamted = moment(myStory.data[0].date).format('MMM DD, YYYY HH:mm');

										// Fill Popup For with Data
										$($("#crimename")[0]).html(
										crimeObj.name +'<span class="crimeShowDate">'
										+ crimeDateForamted +'</span>');

										if(data.author == null || data.author == "")
										{
											$($("#storyInfo")[0]).html(data.story);
										}
										else
										{
											$($("#storyInfo")[0]).html(
											'<i>Por <span class="italicBold">'
											+ data.author +'.</span></i> '
											+ data.story);
										}

										$($("#storyLocation")[0]).html(
										'<i><span class="italicBold">Ocurrió en</span></i> <span>'
										+ myStory.data[0].address + '</span> <a href="'
										+ seeMapLink + '" target="_blank">ver mapa</a>');

										var twitterAppend = '<div class="crimeShowIcon">\
											<span class="socialMediaBtn btnTwitter">\
												<a href="https://twitter.com/intent/tweet?\
												original_referer=https%3A%2F%2Finformant.es%2F%3Fid%3D12345&\
												ref_src=twsrc%5Etfw&text='+ ( data.author = (data.author == null || data.author == "") ? "Anónimo" : data.author )  +'%20ha%20compartido%20una%20historia%20de%20la%20ciudad%20de%20%23medellin%20en%20%23informantes%20&\
												tw_p=tweetbutton&url=https%3A%2F%2Finformant.es%2F%3Fid%3D'+ myStory.data[0].id +'" target="_blank" class="twitter-share-button" data-text="" data-dnt="true">twitter</a>\
											</span>\
											<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');\
											</script>\
										</div>';

										$(".historySocialMedia *").remove();
										$(".historySocialMedia").append(twitterAppend);

										$($("#storyCity")[0]).html(
										'#'+decodeURI(theCity.latinize()));
										var targetStoryCity = "openStoriesLocation(this, '"
											+ myStory.data[0].latitude + "', '"
											+ myStory.data[0].longitude + "', '"
											+ theCity.latinize() + "', '"
											+ theCountry.latinize() + "' )";


										$("#storyCity").attr("onclick", targetStoryCity);

										emojiBoxes.each(function(key, emoji)
										{
											var keyEmoji = (key + 1)
											var targetEmoji = "emoji" + keyEmoji;
											var targetEventOnClick = "addCounterPostStory('myReactions', '"
											+ keyEmoji + "', "+ myStory.data[0].id + ")";
											$(emoji).removeAttr("onclick", targetEventOnClick);
											$(emoji).children("."+targetEmoji).attr("val", 0).text(0);
											$(emoji).attr("onclick", targetEventOnClick);
										});

									}, 1000);
								}
								else
								{
									swal({
										title: "Error al registrar el caso",
										text: "code = " + response.code + " " + response.error,
										type: "error",
										showCancelButton: false,
										confirmButtonColor: "#339bc0",
										confirmButtonText: "Cerrar",
										customClass: "swalCustom",
										closeOnConfirm: false
									});
									isBusy = false; return false;
								}

							},
							error: function(error)
							{
								swal({
									title: "Error al registrar el caso",
									text: error,
									type: "error",
									showCancelButton: false,
									confirmButtonColor: "#339bc0",
									confirmButtonText: "Cerrar",
									customClass: "swalCustom",
									closeOnConfirm: false
								});
								isBusy = false; return false;
							}

						});
					}
				},
				error: function(error)
				{
					swal({
						title: "Error al validar el recaptcha",
						text: error,
						type: "error",
						showCancelButton: false,
						confirmButtonColor: "#339bc0",
						confirmButtonText: "Cerrar",
						customClass: "swalCustom",
						closeOnConfirm: false
					});
					isBusy = false; return false;
				}
			});

			isBusy = false;
			return false;
		}
		else
		{

			swal({
				title: "Falta información",
				text: "Todos los campos son requeridos para registrar tu historia",
				type: "warning",
				showCancelButton: false,
				confirmButtonColor: "#339bc0",
				confirmButtonText: "Cerrar",
				customClass: "swalCustom",
				closeOnConfirm: false
			});
			isBusy = false;
			return false;
		}
	}
	else
	{
		isBusy = false;
		return false;
	}
}

function closeModal() {
	setTimeout(function()
	{
		$("#crime").val("0");
		$("#name").val("");
		$("#story").val("");
		grecaptcha.reset();
		$('html#informant').removeClass('overflowHidden');
	}, 500);
}

// Call After googleapis Load
function initAutocomplete()
{
	var map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 6.16, lng: -75.58},
		zoom: 13,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

	var markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			thePlace = place;
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				icon: icon,
				title: place.name,
				position: place.geometry.location
			}));

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}

			var totalLength;
			theCountry = null;
			theCity = null;

			// Try to get correct city and country
			try
			{
				totalLength = thePlace.address_components.length - 1;
				for (var i=0; i < thePlace.address_components.length; i++)
				{
					for (var j=0; j < thePlace.address_components[i].types.length; j++)
					{
		            	if (thePlace.address_components[i].types[j] == "country" )
		            	{
		              		theCountry = thePlace.address_components[i].long_name;
		            	}

		            	if ( thePlace.address_components[i].types[j] == "locality" )
		            	{
		              		theCity = thePlace.address_components[i].long_name;
		            	}
		          	}
		        }
			}
			catch(e){
				// console.log("La locación seleccionada no es válida");
			}

	        if(!theCountry || !theCity)
	        {
	        	swal({
					title: "La locación seleccionada no es válida.",
					text: "",
					type: "error",
					showCancelButton: false,
					confirmButtonColor: "#339bc0",
					confirmButtonText: "Cerrar",
					customClass: "swalCustom",
					closeOnConfirm: false
				});
				$("#pac-input").val("");
	        }
	        else { globalPlaces = place.geometry.location; }
		});
		map.fitBounds(bounds);
	});

	// Scroll Event listener activate fixed ads
	var enterScrollValidation = false;
	var runOnScroll =  function(evt) {
		if(window.scrollY >= 568 && !enterScrollValidation)
		{
			enterScrollValidation = true;
			$(".ads img").addClass("fixed");
		}
		else if(window.scrollY < 568 && enterScrollValidation)
		{
			enterScrollValidation = false;
			$(".ads img").removeClass("fixed");
		}
	};

	// grab elements as array, rather than as NodeList
	var elements = document.querySelectorAll("html");
	elements = Array.prototype.slice.call(elements);

	function callScrollListener()
	{
		// and then make each element do something on scroll
		elements.forEach(function(element) {
			window.addEventListener("scroll", runOnScroll);
		});
	}

	callScrollListener();

	setTimeout(function callAgain()
	{
		if($('.gm-style')[0] == undefined) { console.log('is undefined '); setTimeout(function() { callAgain(); }, 500); }
		else { $('.gm-style').show(); }
	}, 1000);

}

function noCases () { }

function openApple()
{
	location.href = "https://itunes.apple.com/co/app/informantes/id1224307298?l=en&mt=8";
}

function openAndroid()
{
	swal({
		title: "Muy pronto estará disponible.",
		text: "",
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#339bc0",
		confirmButtonText: "OK",
		customClass: "swalCustom",
		closeOnConfirm: false
	});
}
