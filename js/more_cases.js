
(function() { "use strict"; })();

function showShareSocialMedia(targetId)
{
	var targetShare = $("#"+targetId);
	if(targetShare.hasClass("showSocial")) { targetShare.removeClass("showSocial"); }
	else { targetShare.addClass("showSocial"); }
}

currentPage = sessionStorage.getItem('page');

// Scroll Event listener activate fixed ads
var enterScrollValidation = false;
var runOnScroll =  function(evt) {
	if(window.scrollY >= 36 && !enterScrollValidation)
	{
		enterScrollValidation = true;
		$(".adsMoreCases").addClass("fixed");
	}
	else if(window.scrollY < 36 && enterScrollValidation)
	{
		enterScrollValidation = false;
		$(".adsMoreCases").removeClass("fixed");
	}
};

// grab elements as array, rather than as NodeList
var elements = document.querySelectorAll("html");
elements = Array.prototype.slice.call(elements);

function callScrollListener()
{
	// and then make each element do something on scroll
	elements.forEach(function(element) {
		window.addEventListener("scroll", runOnScroll);
	});
}

callScrollListener();

function noCases(city, country)
{
	swal({
		title: "No hay casos registrados en "+decodeURI(city),
		text: "¿Deseas volver a la página de inicio?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#339bc0",
		cancelButtonColor: "#339bc0",
		confirmButtonText: "Si",
		cancelButtonText: "No",
		closeOnConfirm: false
	},
	function()
	{
		// Reset All Values, Used in Firefox
		
		window.location = 'index.html';   
	});
}

function initAutocomplete() { }

function backToHome() { location.href = "index.html"; }