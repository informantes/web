
(function() { "use strict"; })();

$.ajax({
	type: "GET",
	url: urlBase + "ip_info.php",
	crossDomain: true,
	success: function(response)
    {
        ispIP = response;
        /*
        var endPointCheckLocationByIP = "http://ip-api.com/json/" + ispIP + "?fields=520191";
        $.ajax({
            type: "GET",
            url: endPointCheckLocationByIP,
            crossDomain: true,
            success: function(locationInfo) { locationData = locationInfo; checkBlockCountries(); },
            error: function(error) { console.log(error); }
        });
        */
    },
    error: function(error) { console.log(error); }
});

function showPosition(position)
{
    console.log("Call for Lat and Lng");
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;

    var isGetAPIByIP = false;
    callAPIGETStories(isGetAPIByIP);
}

// User Deny use Geolocation - Error Cases
function errorCallback(error)
{
    var errors = { 
        1: 'Permission denied',
        2: 'Position unavailable',
        3: 'Request timeout'
    };
    console.log("Error: " + errors[error.code]);

    if (error.code == 1 || error.code == 2 || error.code == 3)
    {
    	// Defined in Main JS
    	var isGetAPIByIP = true;
        callAPIGETStories(isGetAPIByIP);
    }
}


function validateGeoLocation()
{
    /*
    Comment here by requirement, access for ip will be always for now.

    if (navigator.geolocation)
    {
        
        if (location.protocol == 'https:')
        {
            
            // Simulate position
            // var position = { coords: { latitude: 6.1696109, longitude: -75.5873636 } };
            // showPosition(position);
            
            
            var timeoutVal = 10 * 1000 * 1000;
            navigator.geolocation.getCurrentPosition(
                showPosition, 
                errorCallback,
                { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 });
            
        }
        else
        {
            //console.log("Is http");
            // console.log("Let´s call callAPIGETStories by IP");
            var isGetAPIByIP = true;
            callAPIGETStories(isGetAPIByIP);
        }

    }
    else
    {
        var isGetAPIByIP = true;
        callAPIGETStories(isGetAPIByIP);
    }
    */

    // Always send data by IP
    var isGetAPIByIP = true;
    callAPIGETStories(isGetAPIByIP);
}

// Call Geolocataion call
validateGeoLocation();

// Get information for country and city

function initialize(lat,lng) {
    var latlng;
    try
    {
        latlng = new google.maps.LatLng(lat, lng);
        getLocation(latlng);
    }
    catch(e)
    {
        setTimeout(function()
        {
            initialize(lat,lng);
        }, 1000);
    }
}

function getLocation(latlng){
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status)
    {
    	if (status == google.maps.GeocoderStatus.OK) { if (results[0]) { var whereIAm = getCountry(results); } }
    });
}

function getCountry(results)
{
	var locationObj = { country: null, city: null };
    for (var i = 0; i < results[0].address_components.length; i++)
    {
        var shortname = results[0].address_components[i].short_name;
        var longname = results[0].address_components[i].long_name;
        var type = results[0].address_components[i].types;

        if (type.indexOf("country") != -1)
        {
            if (!isNullOrWhitespace(shortname)) { locationObj.country = longname; }
            else { locationObj.country = shortname; }
        }

        if(type.indexOf("locality") != -1)
        {
        	if (!isNullOrWhitespace(shortname)) { locationObj.city = longname; }
            else { locationObj.city = shortname; }
        }
    }
    return locationObj;

}

function isNullOrWhitespace(text) {
    if (text == null) { return true; }
    return text.replace(/\s/gi, '').length < 1;
}